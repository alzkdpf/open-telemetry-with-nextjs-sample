'use client';

import { useEffect } from 'react';
import { fetchGithubStars } from '../shared/fetch-github-stars';

export default async function Page() {
  const stars = await fetchGithubStars();

  useEffect(() => {
    // 컴포넌트가 마운트될 때 강제로 에러를 발생시킵니다.
    throw new Error('클라이언트 사이드 에러 발생!');
  }, []);

  const clickError = () => {
    console.log('click');
  };
  return (
    <p>
      Next.js has {stars} ⭐️<button onClick={clickError}>error</button>
    </p>
  );
}
