const {
  BaselimeSDK,
  VercelPlugin,
  BetterHttpInstrumentation,
} = require("@baselime/node-opentelemetry");

import { Resource } from "@opentelemetry/resources";
import { SEMRESATTRS_SERVICE_NAME } from "@opentelemetry/semantic-conventions";

const sdk = new BaselimeSDK({
  serverless: true,
  service: "test",
  resources: new Resource({
    [SEMRESATTRS_SERVICE_NAME]: "next-app",
  }),
  instrumentations: [
    new BetterHttpInstrumentation({
      plugins: [
        // Add the Vercel plugin to enable correlation between your logs and traces for projects deployed on Vercel
        // new VercelPlugin(),
      ],
    }),
  ],
});

sdk.start();
